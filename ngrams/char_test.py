import sys
import numpy as np
import time
from ..parser.text_to_data import TextToData
from predict_ngrams import NgramsPredictor

chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-+.,:;?/\\!@#$%&*()"\'\n ^'

def create_ngrams(sufix):
    ngrams = []
    for char in chars:
        ngrams.append(''.join([sufix, char]))
    return ngrams

def cross_entropy(p,q):
    #    print(q[range(len(p)),p])
    return -np.mean(np.log(q[range(len(p)),p]))

def test_char_ngrams(test_file, n, model_file, n_steps, n_seq):
    model = NgramsPredictor()
    model.load_ngrams_db(model_file)

    t2d = TextToData(n_steps=n_steps, filename=test_file)
    textlines = t2d.get_textlines_from_file(n_seq, length=n_steps)
    errors = np.zeros(n_seq)
#    print('length errors = %i' %len(errors))
    prob = np.zeros([n_steps-n+1, len(chars)])
    line_number = 1
    while len(textlines) == n_seq:
        for i in range(n_seq):
            text = textlines[i]
            index = t2d.from_string_to_id(text)
            for j in range(n_steps-n+1):
                sufix = text[j:j+n-1]
                ngrams = create_ngrams(sufix)
                freq = model.get_freq_exact_ngrams(ngrams)
                prob[j] = model.add_one_smoothing_from_freq(freq)
#            print('text = %s' %(text))
#            print('length errors = %i' %len(errors))
            errors[i] = cross_entropy(index[n-1:], prob)
        print('%i,%f' %(line_number,np.mean(errors)))
        textlines = t2d.get_textlines_from_file(n_seq, length=n_steps)
        line_number = line_number + n_seq

if __name__ == "__main__":
    t0 = time.time()

    if len(sys.argv) != 6:
        sys.stderr.write('Usage(1): {0} <n> <n_steps> <n_seq> <model_file> <test_file>' \
                         '\n'.format(sys.argv[0]))

        exit()

    n = int(sys.argv[1])
    n_steps = int(sys.argv[2])
    n_seq = int(sys.argv[3])
    model_file = sys.argv[4]
    test_file = sys.argv[5]

    test_char_ngrams(test_file=test_file, n=n, model_file=model_file,
                     n_steps=n_steps, n_seq=n_seq)
