from ..predict_ngrams import NgramsPredictor
import sys

ngrams_db = '../example/wiki_mini_6grams.csv'
prefix = 'canis'
sufix = 't'
V = 50**6

if __name__ == '__main__':
    if len(sys.argv) == 4:
        ngrams_db = sys.argv[1]
        prefix = sys.argv[2]
        sufix = sys.argv[3]
 
    predictor = NgramsPredictor()
    predictor.set_vocabulary_size(V)
    predictor.load_ngrams_db(ngrams_db)
    mle = predictor.maximum_likelihood_estimation(sufix,prefix)
    aos = predictor.add_one_smoothing(sufix,prefix)

    print(('maximum likelihood = %f\n' \
          'add one smoothing = %f') % (mle, aos))
