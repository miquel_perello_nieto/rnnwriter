from __future__ import division
import sys
import nltk
from collections import Counter
import csv
import string
import re
import operator
import numpy as np

class NgramsPredictor(object):
    def __init__(self):
        """Initializes the internal variables
        ngrams = dictionary with all ngrams
        n = size of grams
        last_prediction = last predicted ngram
        z = normalization constant
        V = Vocabulary size
        """
        self.ngrams = None 
        self.n = None
        self.last_prediction = None 
        self.z = None
        # FIXME: think about vocabulary size
        self.V = None

    def load_ngrams_db(self, filename):
        """Read the ngrams containeds in a csv file"""
        with open(filename, 'rb') as csv_file:
            bigrams_reader = csv.reader(csv_file, 
                                        delimiter=',', 
                                        quotechar='"')
            self.ngrams = Counter()
            for [key, val] in bigrams_reader:
                self.ngrams[key] = int(val)

            self.n = len(key)

    def load_initial_text(self, filename):
        """Load the initial text from a file"""
        with open(filename, 'rb') as initial_file:
            self.text_begining = initial_file.readline().strip('\n')

    def set_vocabulary_size(self, V):
        self.V = V

    def find_ngrams(self, prefix):
        """Find the ngrams that ends with prefix
        excluding the last character of the
        ngram analysed
        """
        return {k:v for k,v in self.ngrams.iteritems() 
                if k[:-1].endswith(prefix)}

    def get_freq_exact_ngrams(self, ngrams):
        freq = np.zeros(len(ngrams))
        for i in range(len(ngrams)):
            freq[i] = self.ngrams[ngrams[i]]
        return freq

    def most_probable_ngrams(self,prefix,n=None):
        """
        Return the n most probable ngrams given prefix,
        it prefix is None, then it returns the most probable
        ngrams on the database.
        If n is None return all of the results
        """
        if prefix == None:
            options = self.ngrams
        else:
            options = self.find_ngrams(prefix)

        if not options:
            return None
        
        sorted_options = sorted(options.iteritems(), 
                                key=operator.itemgetter(1),
                                reverse=True)

        return sorted_options[0:n]

    def predict(self, prefix=None):
        """Returns the most probable ngram starting by prefix
        If prefix = None, it takes the last prediction as a 
        prefix.
        If there is not a last prediction it takes the last
        ngram from the initial text.
        """
        if prefix == None:
            if self.last_prediction == None:
                prefix = self.text_begining[-self.n+1:]
            else:
                prefix = self.last_prediction[1:]

        iteration = 0
        ngram = None
        while ngram == None or iteration == 0:
            ngram = self.most_probable_ngrams(
                                    prefix[iteration:],
                                    1)
            iteration = iteration + 1

        self.last_prediction = ngram[0][0]
        return self.last_prediction 

    def mle_from_freq(self, freq):
        return freq/np.sum(freq)

    def add_one_smoothing_from_freq(self, freq):
        return (freq+1)/np.sum(freq+1)

    def maximum_likelihood_estimation(self, last_char, prefix):
        """Computes maximum likelihood estimation
        p(w_n | w_1,...,w_n-1) = 
        = count(w_1,...,w_n)/count(w_1,...,w_n-1)
        = particular/general
        """
        general = self.find_ngrams(prefix)
        particular = {k:v for k,v in general.iteritems() 
                      if k.startswith(''.join([prefix,last_char]))}
        
        count_general = sum(general.values())
        count_particular = sum(particular.values())

        if count_particular == 0:
            return 0

        return count_particular/count_general

    def add_one_smoothing(self, last_char, prefix):
        """Computes MLE using laplace add-one smoothing 
        p(w_n | w_1,...,w_n-1) = 
        = (count(w_1,...,w_n)+1)/(count(w_1,...,w_n-1)+V)
        = (particular+1)/(general+V)
        Where V is the size of the vocabulary
        """
        general = self.find_ngrams(prefix)
        particular = {k:v for k,v in general.iteritems() 
                      if k.startswith(''.join([prefix,last_char]))}
        
        count_general = sum(general.values()) + self.V
        count_particular = sum(particular.values()) + 1

        print('dividing %f/%f' % (count_particular, count_general))
        return count_particular/count_general

    def __str__(self):
        return (('N-grams predictor: \n'
                 '# ngrams = %i \n'
                 'ngrams size = %i \n'
                 'text_begining = %s'
                 ) % (len(self.ngrams), 
                     self.n,
                     self.text_begining))
       

if __name__ == '__main__':
    if len(sys.argv) < 4:
        sys.stderr.write('Usage: %s <ngrams_csv> <m> ' \
                         '<input_file>\n' % (sys.argv[0]))
        exit()
   
    ngrams_db = sys.argv[1]
    m = int(sys.argv[2])
    in_filename = sys.argv[3]

    predictor = NgramsPredictor()
    # Load n-grams database
    predictor.load_ngrams_db(ngrams_db)
    # Load initial text 
    predictor.load_initial_text(in_filename)
    print(predictor)
    for i in range(0,m):
        next_ngram = predictor.predict()
        sys.stdout.write(next_ngram[-1])
        sys.stdout.flush()
    print('\n\nPrediction done')

