import sys
import nltk
from collections import Counter
import csv
import string
import re

class NgramsExtractor(object):
    def __init__(self,n):
        self.n = n
        self.unknown = '^'
        self.valid_symbols = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-+.,:;?/\\!@#$%&*()"\'\n '
        self.V = len(self.valid_symbols)**self.n

    def read_file(self, filename):
        """Returns a list with all the content of the file"""
        with open(filename, 'r') as f:
            lines = f.readlines()
        return lines

    def extract_ngrams_freq(self, lines, n=None):
        """Extract ngrams of size n from the list lines"""
        # FIXME: sometimes a quote is added
        if not n:
            n = self.n

        print('Extracting {0}-grams\n'.format(self.n))
        grams = Counter()

        for line in lines:
            clean_line = re.sub(self.valid_symbols,
                                self.unknown, line)
            bgs = list(nltk.ingrams(clean_line,self.n))

            bgs = [''.join(sublist) for sublist in bgs]

            grams.update(bgs)

        return grams

    def export_ngrams_freq(self, grams, filename):
        """Export to a csv file all the ngrams"""
        print('Exporting n-grams to file: {0}\n'
                .format(filename))

        with open(filename, "w") as f:
            w = csv.writer(f, delimiter=',', quotechar='"',
                           quoting=csv.QUOTE_NONNUMERIC)

            for [key, val] in grams.items():
                w.writerow([key, val])

    def print_ngrams(self, lines, n=None):
        """Extract ngrams of size n from the list lines"""
        # FIXME: sometimes a quote is added
        if not n:
            n = self.n

        for line in lines:
            clean_line = re.sub(self.valid_symbols,
                                self.unknown, line)
            bgs = list(nltk.ingrams(clean_line,self.n))
            bgs = [''.join(sublist) for sublist in bgs]
            for line in bgs:
                print (line)

    def __str__(self):
        return ("N-grams extractor: \n"
                "N = {0} \n" "Symbols = {1} \n"
                "Unknown = {2}").format(
                                    self.n,
                                    self.valid_symbols,
                                    self.unknown)

if __name__ == '__main__':
    if len(sys.argv) < 3:
        sys.stderr.write('Usage(1): {0} <n> <input_file> ' \
                         '\n'.format(sys.argv[0]))
        sys.stderr.write('Usage(2): {0} <n> <input_file> ' \
                         '<output_file>\n'.format(sys.argv[0]))
        exit()

    n = int(sys.argv[1])
    in_filename = sys.argv[2]

    # Create an N-grams extractor
    nge = NgramsExtractor(n)
    # Ask for lines of one file
    lines = nge.read_file(in_filename)

    if len(sys.argv) == 3:
        nge.print_ngrams(lines)
    elif len(sys.argv) == 4:
        out_filename = sys.argv[3]
        # Extract ngrams from text lines
        ngrams = nge.extract_ngrams_freq(lines)
        # Export n-grams to a file
        nge.export_ngrams_freq(ngrams, out_filename)

