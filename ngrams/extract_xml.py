import os
import sys
from bs4 import BeautifulSoup

class XmlExtractor(object):
    def __init__(self,tag='body'):
        self.tag = tag 

    def read_file(self, filename):
        with open(filename, 'r') as f:
            self.soup = BeautifulSoup(f) 

    def extract_text_from_tags(self):
        self.texts = [a.get_text() for a in self.soup.find_all(self.tag)]

    def export_texts(self, filename):
        print('Exporting texts to file: {0}\n'
                .format(filename))

        with open(filename, "a") as w:
            for paragraph in self.texts:
                w.write(paragraph.encode("utf8")+'\n\n')

if __name__ == '__main__':
    if len(sys.argv) < 4:
        sys.stderr.write('Usage: {0} <tag> <input_file> ' \
            '<output_file>\n'.format(sys.argv[0]))
        exit()
   
    tag = sys.argv[1]
    out_filename = sys.argv[3]

    # Create an xml extractor
    xmle = XmlExtractor(tag)
    
    path = sys.argv[2]
    if os.path.isfile(path):
        in_filename = path
        # Ask for lines of one file
        xmle.read_file(in_filename)
        xmle.extract_text_from_tags()
        xmle.export_texts(out_filename)
    elif os.path.isdir(path):
        rootdir = path 
        out_filename = os.path.join(os.getcwd(), out_filename)
        print "outfileName is " + out_filename
        with open(out_filename, "w") as fout:
            for root, subFolders, files in os.walk(rootdir):
                for filename in files:
                    if filename.endswith('xml'):
                        filePath = os.path.join(root, filename)
                        xmle.read_file(filePath)
                        xmle.extract_text_from_tags()
                        for paragraph in xmle.texts:
                            fout.write(paragraph.encode("utf8")+'\n\n')
