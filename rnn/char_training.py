import sys, getopt
import numpy as np
from rnn import logging
from rnn import MetaRNN
import time
from char_rnn_basic import basic_RNN
from ..parser.text_to_data import TextToData

INIT_SEEK = 0
NUM_EPOCHS = 2
N_HIDDEN = 300
N_STEPS = 50
N_SEQ = 1000
SAVE_INTERVAL = 25
L1_reg = 0
L2_reg = 0

def get_arguments(argv):
    inputfile = ''
    backups_path = './'
    mfile = None
    hidden = N_HIDDEN 
    try:
        opts, args = getopt.getopt(argv,"yh:i:b:m:",["ifile=","backups=","mfile="])
    except getopt.GetoptError:
        print 'char_training.py -h <hidden> -i <inputfile> -b <backups_path> -m <modelfile>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-y':
            print 'char_training.py -h <hidden> -i <inputfile> -b <backups_path> -m <modelfile>'
            sys.exit()
        elif opt in ("-h", "--hidden"):
            hidden = arg
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-b", "--backups"):
            backups_path = arg
        elif opt in ("-m", "--mfile"):
            mfile = arg
    return [hidden, inputfile, backups_path, mfile]

def train_rnn(n_hidden=500, n_steps=10, n_seq=1000, inputfile='textfile.txt',
              n_epochs=2, n_fit=None, init_seek=0, modelfile=None,
              L1_reg=0, L2_reg=0, backups_path='./'):
    """ Test RNN with softmax outputs. """
    np.random.seed(0)

    f = TextToData(filename=inputfile, n_steps=n_steps, init_seek=init_seek)
    n_in = f.n_in
    n_classes = n_in
    n_out = n_in

    model = MetaRNN(n_in=n_in, n_hidden=n_hidden, n_out=n_out,
                    learning_rate=0.001, learning_rate_decay=1,
                    n_epochs=n_epochs, activation='tanh',
                    L1_reg=L1_reg, L2_reg=L2_reg,
                    output_type='softmax', use_symbolic_softmax=True)

    if modelfile != None:
        model.load(modelfile)
    
    print(model)
    sys.stdout.flush()

    stop_condition = False
    i = 0
    while not stop_condition:
        seq, targets = f.get_rnn_samples_from_file(n_seq)
        if n_fit == None:
            if len(targets) < n_seq:
                stop_condition = True
        elif n_fit <= 0:
            stop_condition = True
        else:
            n_fit = n_fit-1

        if not stop_condition:
            model.fit(seq, targets, validation_frequency=100)
            if i == 0:
                model.save(backups_path)
            i = (i+1)%SAVE_INTERVAL

    model.save(backups_path)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    t0 = time.time()

    [hidden, inputfile, backups_path, model_file] = get_arguments(sys.argv[1:])

    train_rnn(n_hidden=hidden, n_steps=N_STEPS, n_seq=N_SEQ,
              n_epochs=NUM_EPOCHS, modelfile=model_file, inputfile=inputfile,
              init_seek=INIT_SEEK, backups_path=backups_path, L1_reg=L1_reg,
              L2_reg=L2_reg)
    print("Elapsed time: %f" % (time.time() - t0))

