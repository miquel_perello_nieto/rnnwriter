from text_to_data import TextToData
f = TextToData(filename='../data/clean_text/wiki_test.txt', n_steps=50)
sequence, target = f.get_rnn_samples_from_file(2,4)

print("sequence dimensions = [%i,%i,%i], target dimensions = [%i,%i]" %(len(sequence),len(sequence[0]),len(sequence[0][0]), len(target),len(target[0])))
print(sequence)
print(target)

for sample in target:
    print(f.from_id_to_string(sample))
