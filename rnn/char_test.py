import sys
import numpy as np
from rnn import logging
from rnn import MetaRNN 
import time 
from char_rnn_basic import basic_RNN
from ..parser.text_to_data import TextToData

EVALUATE = 20

def cross_entropy(p,q):
    p = p[-EVALUATE:]
    q = q[-EVALUATE:]
    return -np.mean(np.log(q[range(len(p)),p]))

def test_char(test_file, model_file=None):
    """ Test RNN with softmax outputs. """
    n_hidden = 300 
    n_steps = 50
    n_seq = 100 

    np.random.seed(0)
    
    n_classes = 86 
    n_in = n_classes 
    n_out = n_in
 

    model = MetaRNN(n_in=n_in, n_hidden=n_hidden, n_out=n_out,
                    learning_rate=0.001, learning_rate_decay=0.999,
                    n_epochs=1, activation='tanh',
                    output_type='softmax', use_symbolic_softmax=True)

    if model_file != None:
        model.load(model_file)

    t2d = TextToData(n_steps=n_steps, filename=test_file)
    seq, target = t2d.get_rnn_samples_from_file(n_seq)
    errors = np.zeros(n_seq)
    while len(target) >= n_seq:
        for i in range(n_seq):
            prob = model.predict_proba(seq[i]) 
            errors[i] = cross_entropy(target[i], prob)
        print(np.mean(errors))
        seq, target = t2d.get_rnn_samples_from_file(n_seq)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    t0 = time.time()
    
    if len(sys.argv) != 3:
        sys.stderr.write('Usage(1): {0} <model_file> <test_file>' \
                         '\n'.format(sys.argv[0]))
   
        exit()

    model_file = sys.argv[1]
    test_file = sys.argv[2]

    test_char(test_file=test_file, model_file=model_file)
