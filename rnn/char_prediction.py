import sys
import numpy as np
from rnn import logging
from rnn import MetaRNN 
import time 
from char_rnn_basic import basic_RNN
from ..parser.text_to_data import TextToData

def predict_char(initial_text, length, model_file=None):
    """ Test RNN with softmax outputs. """
    n_hidden = 500 
    n_steps = 10 
    n_seq = 1000

    np.random.seed(0)
    
    n_classes = 86 
    n_in = n_classes 
    n_out = n_in
 

    model = MetaRNN(n_in=n_in, n_hidden=n_hidden, n_out=n_out,
                    learning_rate=0.001, learning_rate_decay=0.999,
                    n_epochs=1, activation='tanh',
                    output_type='softmax', use_symbolic_softmax=True)

    if model_file != None:
        model.load(model_file)

    t2d = TextToData()
    text = initial_text
    sys.stdout.write(text)
    sys.stdout.flush()
    for i in xrange(length):
        seq = t2d.get_seq_from_text(text[-n_steps])

        prediction = model.predict(seq)

        char = t2d.from_id_to_string(prediction)

        text = text + char

        sys.stdout.write(char)
        sys.stdout.flush()

    sys.stdout.write('\n')

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    t0 = time.time()
    
    if len(sys.argv) != 4:
        sys.stderr.write('Usage(1): {0} <model_file> <initial_text> <length> ' \
                         '\n'.format(sys.argv[0]))
   
        exit()

    model_file = sys.argv[1]
    text = sys.argv[2]
    length = int(sys.argv[3])

    predict_char(initial_text=text, length=length, model_file=model_file)

