import numpy as np
import theano
import theano.tensor as TT
from collections import OrderedDict
import logging

logger = logging.getLogger(__name__)

logging.basicConfig(level=logging.INFO)

class basic_RNN(object):
    def __init__(self, n_in, n_out, n_hidden, activation='tanh'):
        self.n_in = n_in
        self.n_out = n_out
        self.n_hidden = n_hidden

        if activation == 'tanh':
            self.activation = TT.tanh
        else:
            self.activation = TT.tanh

        self.output_type = 'softmax'

        self.L1_reg = 0.1

        self.create()

    def create(self):
        # input (where first dimension is time)
        self.u = TT.matrix()
        # target (where first dimension is time)
        self.t = TT.matrix()
        # initial hidden state of the RNN
        self.h0 = TT.vector()
        # learning rate
        self.lr = TT.scalar()
        # recurrent weights as a shared variable
        self.W = theano.shared(np.random.uniform(
                    size=(self.n_hidden, self.n_hidden), 
                    low=-.01, high=.01))
        # input to hidden layer weights
        self.W_in = theano.shared(np.random.uniform(
                    size=(self.n_in, self.n_hidden), 
                    low=-.01, high=.01))
        # hidden to output layer weights
        self.W_out = theano.shared(np.random.uniform(
                    size=(self.n_hidden, self.n_out), 
                    low=-.01, high=.01))

        # L1 norm ; one regularization option is to enforce L1 norm to
        # be small
        self.L1 = 0
        self.L1 += abs(self.W.sum())
        self.L1 += abs(self.W_in.sum())
        self.L1 += abs(self.W_out.sum())

        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, h_tm1, W, W_in, W_out):
            h_t = self.activation(TT.dot(u_t, W_in) + TT.dot(h_tm1, W))
            y_t = TT.dot(h_t, W_out)
            return h_t, y_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [self.h, self.y], _ = theano.scan(step,
                                sequences=self.u,
                                outputs_info=[self.h0, None],
                                non_sequences=[self.W, self.W_in, self.W_out])
       
        self.p_y_given_x = TT.nnet.softmax(self.y)
        # error between output and target
        #error = ((y - t) ** 2).sum()
        # FIXME: is it correct to average?
        self.error = TT.mean(TT.nnet.categorical_crossentropy(self.p_y_given_x, self.t))
        # gradients on the weights using BPTT
        self.gW, self.gW_in, self.gW_out = TT.grad(self.error, [self.W, self.W_in, self.W_out])
        # training function, that computes the error and updates the weights using
        # SGD
        updates = OrderedDict({self.W: self.W - self.lr * self.gW,
                               self.W_in: self.W_in - self.lr * self.gW_in,
                               self.W_out: self.W_out - self.lr * self.gW_out})
        self.fn = theano.function([self.h0, self.u, self.t, self.lr],
                             self.error,
                             updates=updates)
    
    def shared_dataset(self, data_xy):
        """ Load the dataset into shared variables """

        data_x, data_y = data_xy
        shared_x = theano.shared(np.asarray(data_x,
                                            dtype=theano.config.floatX))

        shared_y = theano.shared(np.asarray(data_y,
                                            dtype=theano.config.floatX))

        if self.output_type in ('binary', 'softmax'):
            return shared_x, TT.cast(shared_y, 'int32')
        else:
            return shared_x, shared_y

    def nll_multiclass(self, y):
        # negative log likelihood based on multiclass cross entropy error
        # y.shape[0] is (symbolically) the number of rows in y, i.e.,
        # number of time steps (call it T) in the sequence
        # T.arange(y.shape[0]) is a symbolic vector which will contain
        # [0,1,2,... n-1] T.log(self.p_y_given_x) is a matrix of
        # Log-Probabilities (call it LP) with one row per example and
        # one column per class LP[T.arange(y.shape[0]),y] is a vector
        # v containing [LP[0,y[0]], LP[1,y[1]], LP[2,y[2]], ...,
        # LP[n-1,y[n-1]]] and T.mean(LP[T.arange(y.shape[0]),y]) is
        # the mean (across minibatch examples) of the elements in v,
        # i.e., the mean log-likelihood across the minibatch.
        return -TT.mean(TT.log(self.p_y_given_x)[TT.arange(y.shape[0]), y])



    def fit(self, X_train, Y_train):
        print('Fitting the data')
        train_set_x, train_set_y = self.shared_dataset((X_train, Y_train))

        n_train = train_set_x.get_value(borrow=True).shape[0]
        
        ######################
        # BUILD ACTUAL MODEL #
        ######################
        logger.info('... building the model')

        index = TT.lscalar('index')    # index to a case

