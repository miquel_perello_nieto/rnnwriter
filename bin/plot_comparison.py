import sys
import pickle
from extract_statistics import  StatisticsExtractor
from matplotlib import pyplot as plt

if __name__ == '__main__':
    if len(sys.argv) < 3:
        sys.stderr.write('Usage(2): {0} <statistics_jrc> ' \
                         '<statistics_wiki>\n'.format(sys.argv[0]))
        exit()
    
    jrc_filename = sys.argv[1]
    wiki_filename = sys.argv[2]
   
    jrc_st = pickle.load(open(jrc_filename, 'rb'))
    wiki_st = pickle.load(open(wiki_filename, 'rb'))

    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.plot(jrc_st.word_len.keys(), jrc_st.word_len.values(), 'bo', wiki_st.word_len.keys(), wiki_st.word_len.values(), 'rx')
    ax.set_yscale('log')
    ax.set_xscale('log')
    plt.legend(('jrc_es', 'wiki_en'))
    plt.xlabel('log(word length)')
    plt.ylabel('log(frequency)')
    plt.show()
    
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.plot(jrc_st.line_len.keys(), jrc_st.line_len.values(), 'bo', wiki_st.line_len.keys(), wiki_st.line_len.values(), 'rx')
    ax.set_yscale('log')
    ax.set_xscale('log')
    plt.legend(('jrc_es', 'wiki_en'))
    plt.xlabel('log(line length)')
    plt.ylabel('log(frequency)')
    plt.show()

    jrc_st.plot_char_bars(color='b')
    wiki_st.plot_char_bars(color='r')
