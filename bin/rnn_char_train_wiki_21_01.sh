#!/bin/bash

EXC_PATH="/u/57/perellm1/unix/git/rpcis/"
INPUT="./data/clean_text/wiki_train.txt"
BACKUPS="./models/wiki_21_01"
OUTPUT="./results/wiki_21_01.log"

cd "$EXC_PATH"
mkdir -p "$BACKUPS"

python -m RNNwriter.rnn.char_training -i "$INPUT" -b "$BACKUPS" &> "$OUTPUT"

