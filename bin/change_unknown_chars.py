import sys

chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-+.,:;?/\\!@#$%&*()"\'\n ^'
unknown = '^'

def clean_text(text):
    new_text = ''
    for c in text:
        if c not in chars:
            new_text += unknown
        else:
            new_text += c
    return new_text

if len(sys.argv) < 3:
    sys.stderr.write('Usage(2): {0} <input_file> ' \
                     '<output_file>\n'.format(sys.argv[0]))
    exit()

in_filename = sys.argv[1]
out_filename = sys.argv[2]

with open(in_filename, 'r') as inf:
    with open(out_filename, 'w') as outf:
        for line in inf.readlines():
            new_line = clean_text(line)    
            outf.write(new_line)
