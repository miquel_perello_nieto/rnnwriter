#!/bin/bash

MODULE_PATH="$HOME/git/rpcis"
MODELS_DIR="$HOME/git/rpcis/data/ngrams"
DATA_DIR="$HOME/git/rpcis/data/clean_text"
OUT_DIR="./train_error"
N_STEPS=50
N_SEQ=100
N_START=4
N_END=4

if ! [ -d "$OUT_DIR" ]; then
    mkdir "$OUT_DIR"
fi

cd $OUT_DIR
OUT_DIR=`pwd`
cd $MODULE_PATH

# TRAIN ERROR
for (( i=$N_START; i<=$N_END; i++))
do
    # English corpus
    echo "Train error of $i-grams on English Corpus"
    python -m "$MODULE_PATH"/RNNwriter.ngrams.char_test "$i" "$N_STEPS" "$N_SEQ" \
        "$MODELS_DIR"/wiki_"$i"grams.csv "$DATA_DIR"/wiki_train.txt \
        > "$OUT_DIR"/wiki_train_error_"$i"grams.csv

    # English and Spanish corpus
    echo "Train error of $i-grams on English/Spanish Corpus"
    python -m "$MODULE_PATH"/RNNwriter.ngrams.char_test "$i" "$N_STEPS" "$N_SEQ" \
        "$MODELS_DIR"/wiki_jrc_"$i"grams.csv "$DATA_DIR"/wiki_jrc_train.txt \
        > "$OUT_DIR"/wiki_jrc_train_error_"$i"grams.csv
done

