#!/bin/bash

MODULE_PATH="$HOME/git/rpcis"
MODELS_DIR="$HOME/git/rpcis/models/ngrams"
DATA_DIR="$HOME/git/rpcis/data/clean_text"
N_START=9
N_END=9

cd $MODULE_PATH

# TRAIN ERROR
for (( i=$N_START; i<=$N_END; i++))
do
    # English corpus
    #echo "Extracting $i-grams on English Corpus"
    #python -m RNNwriter.ngrams.extract_ngrams "$i" \
    #    "$DATA_DIR"/wiki_train.txt "$MODELS_DIR"/eng_"$i"grams.csv &

    # English and Spanish corpus
    echo "Extracting  $i-grams on English/Spanish Corpus"
    python -m RNNwriter.ngrams.extract_ngrams "$i" \
        "$DATA_DIR"/wiki_jrc_train.txt "$MODELS_DIR"/mix_"$i"grams.csv &
done

