import sys
import nltk
from collections import Counter
import csv
import string
import re
import pickle
import array
import matplotlib as mpl
from matplotlib import pyplot as plt


class StatisticsExtractor(object):
    def __init__(self):
        self.unknown = '^'
        self.valid_symbols = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-+.,:;?/\\!@#$%&*()"\'\n '
        self.line_len = Counter()
        self.word_len = Counter() 
        self.char_freq = Counter()

    def extract_freq(self, filename):
        """Extract ngrams of size n from the list lines"""
        # FIXME: sometimes a quote is added 
        n = 1
        print('Extracting statistics\n')
        with open(filename, 'r') as f:
            for line in f.readlines():
                if len(line) != 1:
                    clean_line = re.sub(self.valid_symbols, 
                                        self.unknown, line)
                    bgs = list(nltk.ingrams(clean_line,n))
                   
                    bgs = {''.join(sublist) for sublist in bgs}
                    
                    self.char_freq.update(bgs)
                    self.line_len.update((len(line),))
                    for word in line.split():
                        self.word_len.update((len(word),))
    
    def retrieve_large_words(self, filename, minimum=100, maximum=500):
        """Extract ngrams of size n from the list lines"""
        # FIXME: sometimes a quote is added 
        n = 1
        print('Extracting statistics\n')
        with open(filename, 'r') as f:
            for line in f.readlines():
                for word in line.split():
                    if (len(word) >= minimum) & (len(word) <= maximum):
                        print(word)

    def save(self, filename):
        pickle.dump(self, open(filename, 'wb'))

    def load(self, filename):
        self = pickle.load(open(filename, 'rb'))

    def plot_word_bars(self):
        bars = sorted(self.word_len.items(), key=lambda item: item[0])
        keys, values = self.get_keys_values(bars)
        self.plot_bars(keys,values)

    def plot_line_bars(self):
        bars = sorted(self.line_len.items(), key=lambda item: item[0])
        keys, values = self.get_keys_values(bars)
        self.plot_bars(keys,values)

    def plot_char_bars(self, color='b'):
        bars = sorted(self.char_freq.items(), key=lambda item: item[0])
        keys, values = self.get_keys_values(bars)
        self.plot_bars(keys,values, color=color)

    def get_keys_values(self, bars):
        keys = [str(i[0]).encode("string-escape") for i in bars]
        values = [int(i[1]) for i in bars]
        return keys, values

    def plot_bars(self, keys, values, color='b'):
        plt.bar(range(len(values)), values, color=color)
        plt.xticks(range(len(keys)), keys, rotation=50)
        plt.show()

if __name__ == '__main__':
    if len(sys.argv) < 3:
        sys.stderr.write('Usage(2): {0} <input_file> ' \
                         '<output_file>\n'.format(sys.argv[0]))
        exit()
    
    in_filename = sys.argv[1]
    
    # Create an N-grams extractor
    se = StatisticsExtractor()

    out_filename = sys.argv[2]
    # Extract ngrams from text lines
    se.extract_freq(in_filename)
    # save statistics 
    se.save(out_filename)

    se.plot_char_bars()
    se.plot_word_bars()
    se.plot_line_bars()

