#!/bin/bash

EXC_PATH="/u/57/perellm1/unix/git/rpcis/"
INPUT="./data/clean_text/wiki_jrc_train.txt"
BACKUPS="./models/jrc_15_18"
OUTPUT="./results/jrc_15_18.log"

cd "$EXC_PATH"
mkdir -p "$BACKUPS"

python -m RNNwriter.rnn.char_training -i "$INPUT" -b "$BACKUPS" &> "$OUTPUT"

