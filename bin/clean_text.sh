#!/bin/sh

if [ $# -gt 0 ] ; then
    filename=$1
else
    echo "Usage: $0 <filename>"
    exit
fi

sed 's/á/a/g;s/é/e/g;s/í/i/g;s/ó/o/g;s/ú/u/g;s/ñ/n/g;s/Á/A/g;s/É/E/g;s/Í/I/g;s/Ó/O/g;s/Ú/U/g;s/Ñ/N/g;s/ü/u/g;s/Ü/U/g;s/[0-9].//g' -i $filename

perl -i.orig -00 -ple 's/\s*\n\s*/ /g' $filename
