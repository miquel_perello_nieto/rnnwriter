import sys

if len(sys.argv) < 6:
    sys.stderr.write('Usage(2): {0} <word_max> ' \
                     '<line_min> <line_max> <input_file> ' \
                     '<output_file>\n'.format(sys.argv[0]))
    exit()

word_max = int(sys.argv[1])
line_min = int(sys.argv[2])
line_max = int(sys.argv[3])
in_filename = sys.argv[4]
out_filename = sys.argv[5]

with open(in_filename, 'r') as inf:
    with open(out_filename, 'w') as outf:
        for line in inf.readlines():
            if (len(line) > line_min) & (len(line) < line_max):
                new_line = ''
                for word in line.split():
                    if len(word) < word_max:
                        if new_line == '':
                            new_line = new_line + word
                        else:
                            new_line = new_line + ' ' + word
                if len(new_line) > line_min:
                    outf.write(new_line + '\n')
                    outf.flush()
