#!/bin/sh

for f in `ls ./*.sgm-nobody.txt`
do
    sed 's/ Reuter//g' $f > $f.clean
done
