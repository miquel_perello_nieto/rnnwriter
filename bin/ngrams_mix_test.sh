#!/bin/bash

MODULE_PATH="$HOME/git/rpcis"
MODELS_DIR="$HOME/git/rpcis/models/ngrams"
DATA_DIR="$HOME/git/rpcis/data/clean_text"
OUT_DIR="./test_error"
ACTUAL_DIR=`pwd`
N_STEPS=50
N_SEQ=100
N_START=1
N_END=4

if ! [ -d "$OUT_DIR" ]; then
    mkdir "$OUT_DIR"
fi

cd $OUT_DIR
OUT_DIR=`pwd`
cd $MODULE_PATH

# TEST ERROR
for (( i=$N_START; i<=$N_END; i++))
do
    # English corpus
    echo "Test error of mixed $i-grams on English Corpus"
    python -m RNNwriter.ngrams.char_test "$i" "$N_STEPS" "$N_SEQ" \
        "$MODELS_DIR"/mix_"$i"grams.csv "$DATA_DIR"/wiki_test.txt \
        > "$OUT_DIR"/"$i"grams_mix_test_eng.csv &

    # English and Spanish corpus
    echo "Test error of mixed $i-grams on English/Spanish Corpus"
    python -m RNNwriter.ngrams.char_test "$i" "$N_STEPS" "$N_SEQ" \
        "$MODELS_DIR"/mix_"$i"grams.csv "$DATA_DIR"/wiki_jrc_test.txt \
        > "$OUT_DIR"/"$i"grams_mix_test_mix.csv &

done
