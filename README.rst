==========================================
RNNwriter: Recurrent Neural Network writer
==========================================

:Author: Miquel Perelló Nieto
:Version : 0.1
:Copyright: This document has been placed in the public domain.

Welcome to RRNwriter!

RNNwriter is an implementation of an Artificial Recurrent
Neural Network to create and predict text.

Create n-grams
--------------

::

    This script reads a file with text (input_file) and
    creates a database of n-grams in the specified
    csv file (output_file), the format of this database
    is in the first column the n-grams between double quotes,
    and the second column the frequency.
    Is it possible to specify the size of the N as a parameter.

**command:**
       extract_grams.py <n> <input_file> <output_file>

Predict text with n-grams
-------------------------

::

    This script predicts the next M characters of an initial
    text (input_file) using a n-grams database (ngrams_csv)

**command:**
       predict_ngrams.py <ngrams_csv> <m> <input_file>

Example of both programs
------------------------

::

    Example of use that creates an n-gram database and
    predicts the next characters from a textfile.
    (You are suposed to be in the /example/ directory)

**command:**
        sh example.sh

**expected output:**

::

    N-grams extractor:
    N = 6
    Symbols = [^a-zA-Z0-9


    !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]
    Unknown = #
    Extracting 6-grams

    Exporting n-grams to file: ./wiki_mini_6grams.csv

    N-grams predictor:
    # ngrams = 10193
    ngrams size = 6
    text_begining = The meaning of life is
    their own name to be largely accept a small payment in directing the common service and the common service and the common service and the common servi

    Prediction done
