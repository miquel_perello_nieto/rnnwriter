import sys
from collections import Counter
import numpy as np
import csv
import string
import re

class TextToData(object):
    def __init__(self,n_steps=1,filename=None,init_seek=0):
        """There are 85 recognized characters
           plus one special one '^', then
           the input and output can have 86
           characters or self.n_in
        """
        if filename != None:
            self.filename = filename
        self.n_steps = n_steps
        self.chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-+.,:;?/\\!@#$%&*()"\'\n '
        self.n_chars = len(self.chars)
        self.n_in = self.n_chars+1
        self.unknown = '^'
        self.seek = init_seek
        self.first_line = 0
        self.chars_dict = dict((c,i) for (i,c) in enumerate(self.chars))
        self.chars_inv_dict = dict([(i,c) for (i,c) in enumerate(self.chars)]
                                 + [(len(self.chars), self.unknown)])

    def from_string_to_id(self, string):
        ids = [self.chars_dict.get(ch,self.n_chars) for ch in string]
        return ids

    def from_id_to_string(self, ids):
        string = ''.join([self.chars_inv_dict[int(id)] for id in ids])
        return string

    def set_file(self, filename, seek=0):
        self.filename = filename
        self.seek = seek

    def _index_to_seq(self,index):
        sequence = np.zeros((len(index), self.n_in))
        for i in xrange(len(index)):
            sequence[i,index[i]] = 1

        return sequence

    def get_textlines_from_file(self, num_lines, first_line=None, length=None):
        """Return a list of text phrases

        of the specified number of lines from the
        previously indicated textfile.

        """
        if first_line == None:
            first_line = self.first_line
        textlines = []
        first_line = first_line
        with open(self.filename, 'r') as f:
            for i, line in enumerate(f):
                if (i >= first_line) and (i < first_line + num_lines):
                    if length != None:
                        line = line[0:length]
                    textlines.append(line)
                elif i > (first_line + num_lines):
                    break
        self.first_line = first_line + num_lines
        return textlines

    def _read_file(self, n_bytes):
        """Return the specified number of characters

        from the textfile continuing from the last
        position where it was left

        """
        if self.filename == None:
            print('the filename needs to be set in advance\n')
            exit()

        with open(self.filename, 'r') as f:
            f.seek(self.seek)
            text = f.read(n_bytes)
            self.seek = self.seek + len(text)
        return text

    def get_rnn_samples_from_file(self, num_lines, first_line=None):
        """Returns the specified number of samples for a RNN

        the text file, if first_line is specified it starts from that line

        """
        textlines = self.get_textlines_from_file(num_lines,
                                                 first_line,
                                                 self.n_steps+1)
        # Correct the number of lines avaliable
        num_lines = len(textlines)
        sequences = np.zeros((num_lines, self.n_steps, self.n_in))
        targets = np.zeros((num_lines, self.n_steps), dtype=np.int)
        i = 0
        for line in textlines:
            text = re.sub(self.chars, self.unknown, line)
            index = self.from_string_to_id(text[0:self.n_steps+1])
            for j in range(self.n_steps):
                sequences[i,j,index[j]] = 1
                targets[i,j] = index[j+1]
            i = i + 1
        return sequences, targets

    def get_seq_from_text(self, text):
        """Constructs an input for a RNN

        given the text

        """
        #Text with only desired symbols
        text = re.sub(self.chars,
                            self.unknown, text)

        index = self.from_string_to_id(text)
        sequence = self._index_to_seq(index)
        return sequence


